import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        menu1();
        System.out.print("Pilihan: ");
        String pilihan = input.nextLine();

        if (pilihan.equals("1")) {
            switch (pilihan) {
                case "1":
                    menu2();
                    System.out.print("Pilihan: ");
                    String pilihan2 = input.nextLine();

                    if(pilihan2.equals("1")){
                        persegi();
                    } else if(pilihan2.equals("2")){
                        lingkaran();
                    } else if(pilihan2.equals("3")){
                        segitiga();
                    } else if(pilihan2.equals("4")){
                        persegipanjang();
                    } else if(pilihan2.equals("0")) {
                        menu1();
                    }
                    break;
                default:
                    System.out.println("Pilihannya sesuai diatas");
            }
        }
        else if(pilihan.equals("2")) {
            switch (pilihan) {
                case "2":
                    menu3();
                    String pilihan3 = input.nextLine();

                    if(pilihan3.equals("1")){
                        kubus();
                    } else if(pilihan3.equals("2")){
                        balok();
                    } else if(pilihan3.equals("3")){
                        tabung();
                    } else if(pilihan3.equals("0")) {
                        menu1();
                    }
                    break;
                default:
                    System.out.println("Pilihannya sesuai diatas");
            }
        }
        else if(pilihan.equals("0")){
            System.exit(0);
        }
        else{
            menu1();
        }
    }
    public static void menu1(){
        Scanner input = new Scanner(System.in);
        System.out.println("--------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("--------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas");
        System.out.println("2. Hitung Volume");
        System.out.println("0. Tutup Aplikasi");
        System.out.println("--------------------------------------");

        System.out.print("Pilihan: ");
        int pilihan = input.nextInt();

        if (pilihan == 1) {
            switch (pilihan) {
                case 1:
                    menu2();
                    System.out.print("Pilihan: ");
                    int pilihan2 = input.nextInt();

                    if(pilihan2 == 1){
                        persegi();
                    } else if(pilihan2 == 2){
                        lingkaran();
                    } else if(pilihan2 == 3){
                        segitiga();
                    } else if(pilihan2 == 4){
                        persegipanjang();
                    } else if(pilihan2 == 0) {
                        menu1();
                    }
                    break;
                default:
                    System.out.println("Pilihannya sesuai diatas");
            }
        }
        else if(pilihan == 2) {
            switch (pilihan) {
                case 2:
                    menu3();
                    int pilihan3 = input.nextInt();

                    if(pilihan3 == 1){
                        kubus();
                    } else if(pilihan3 == 2){
                        balok();
                    } else if(pilihan3 == 3){
                        tabung();
                    } else if(pilihan3 == 0) {
                        menu1();
                    }
                    break;
                default:
                    System.out.println("Pilihannya sesuai diatas");
            }
        }
        else if(pilihan == 0){
            //
        }
        else{
            menu1();
        }
    }
    public static void menu2(){
        System.out.println("--------------------------------------");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("--------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. Kembali ke menu sebelumnya");
    }
    public static void menu3(){
        System.out.println("--------------------------------------");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("--------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.print("Pilihan: ");
    }

    public static void persegi(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Nilai Sisi: ");
        float sisi = input.nextInt();

        double luaspersegi;
        luaspersegi = sisi*sisi;
        System.out.println("Luas Persegi: " + luaspersegi);
        System.out.print("Tekan tombol apa saja untuk kembali ke menu utama: ");
        try{
            System.in.read();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static void lingkaran(){
        Scanner input = new Scanner(System.in);
        double phi; phi = 3.14;

        System.out.print("Masukkan Nilai Jari-jari: ");
        float r = input.nextInt();

        double luaslingkaran;
        luaslingkaran = phi*r;
        System.out.println("Luas Lingkaran: " + luaslingkaran);
        System.out.print("Tekan tombol apa saja untuk kembali ke menu utama: ");
        try{
            System.in.read();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static void segitiga(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Nilai Alas: ");
        float a = input.nextInt();
        System.out.print("Masukkan Nilai Tinggi: ");

        float t = input.nextInt();
        double luassegitiga;
        luassegitiga = 0.5*a*t;
        System.out.println("Luas Segitiga: " + luassegitiga);
        System.out.print("Tekan tombol apa saja untuk kembali ke menu utama: ");
        try{
            System.in.read();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static void persegipanjang() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Nilai Panjang: ");
        float p = input.nextInt();
        System.out.print("Masukkan Nilai Lebar: ");
        float l = input.nextInt();

        double luaspersegipanjang;
        luaspersegipanjang = p * l;
        System.out.println("Luas Persegi Panjang: " + luaspersegipanjang);
        System.out.print("Tekan tombol apa saja untuk kembali ke menu utama: ");
        try{
            System.in.read();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void kubus(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Nilai Sisi: ");
        float sisi = input.nextInt();

        double volumekubus;
        volumekubus = sisi*sisi*sisi;
        System.out.println("Volume Kubus: " + volumekubus);
        System.out.print("Tekan tombol apa saja untuk kembali ke menu utama: ");
        try{
            System.in.read();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static void balok(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Nilai Panjang: ");
        float p = input.nextInt();
        System.out.print("Masukkan Nilai Lebar: ");
        float l = input.nextInt();
        System.out.print("Masukkan Nilai Tinggi: ");
        float t = input.nextInt();

        double volumebalok;
        volumebalok = p*l*t;
        System.out.println("Volume Balok: " + volumebalok);
        System.out.print("Tekan tombol apa saja untuk kembali ke menu utama: ");
        try{
            System.in.read();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static void tabung(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Nilai Tinggi: ");
        float t = input.nextInt();
        System.out.print("Masukkan Nilai Jari-Jari: ");
        float r = input.nextInt();

        double volumetabung;
        volumetabung = 0.5*t*r*r;
        System.out.println("Volume Tabung: " + volumetabung);
        System.out.print("Tekan tombol apa saja untuk kembali ke menu utama: ");
        try{
            System.in.read();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
